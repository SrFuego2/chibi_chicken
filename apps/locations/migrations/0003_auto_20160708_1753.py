# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0002_auto_20160707_0315'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'verbose_name': 'Orden', 'verbose_name_plural': '\xd3rdenes'},
        ),
        migrations.RemoveField(
            model_name='order',
            name='saucers',
        ),
        migrations.AddField(
            model_name='order',
            name='quantity',
            field=models.PositiveSmallIntegerField(null=True, verbose_name=b'Cantidad', blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='saucer',
            field=models.ForeignKey(default=1, verbose_name=b'Platillos', to='locations.Saucer'),
            preserve_default=False,
        ),
    ]
