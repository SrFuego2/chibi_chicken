# -*- coding: utf-8 -*-
# Python imports


# Django imports
from django.db import models


# Third party apps imports


# Local imports
from .managers import OrderQuerySet


# Create your models here.
class Saucer(models.Model):

    STATE_CHOICES = (
        ('no disponible', 'No disponible'),
        ('agotado', 'agotado'),
        ('en stock', 'En Stock'),)

    name = models.CharField(max_length=50, verbose_name='Nombre', unique=True)
    price = models.DecimalField(
        max_digits=7, decimal_places=2, verbose_name='Precio')
    state = models.CharField(
        choices=STATE_CHOICES, max_length=50, verbose_name='Estado')
    quantity = models.PositiveSmallIntegerField(verbose_name='Cantidad')

    class Meta:
        verbose_name = 'Platillo'
        verbose_name_plural = 'Platillos'

    def __unicode__(self):
        return '{0} - S/. {1}'.format(self.name, self.price)


class Table(models.Model):

    capacity = models.PositiveSmallIntegerField(
        verbose_name='Cantidad de personas')
    number = models.PositiveSmallIntegerField(
        unique=True, verbose_name='Numero de mesa')

    class Meta:
        verbose_name = 'Mesa'
        verbose_name_plural = 'Mesas'

    def __unicode__(self):
        return '{0}'.format(self.number)


class Order(models.Model):

    STATE_CHOICES = (
        ('solicitado', 'Solicitado'),
        ('preparando', 'Preparando'),
        ('para servir', 'Para servir'),
        ('cancelado', 'Cancelado'),
        ('servido', 'Servido'),)

    table = models.ForeignKey(Table, verbose_name='Mesa')
    saucer = models.ForeignKey(Saucer, verbose_name='Platillo')
    quantity = models.PositiveSmallIntegerField(
        blank=True, null=True, verbose_name='Cantidad')
    state = models.CharField(
        choices=STATE_CHOICES, max_length=50, verbose_name="Estado")

    objects = OrderQuerySet.as_manager()

    class Meta:
        verbose_name = 'Orden'
        verbose_name_plural = u'Órdenes'

    def __unicode__(self):
        return '{0}'.format(self.table.number)
