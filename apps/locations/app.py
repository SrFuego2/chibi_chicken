# -*- coding: utf-8 -*-
# Python imports
from __future__ import unicode_literals


# Django imports
from django.apps import AppConfig


# Third party apps imports


# Local imports


# Configure your app here.
class LocationsConfig(AppConfig):
    name = 'apps.locations'
    verbose_name = 'Sedes'
