# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0003_auto_20160708_1753'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='state',
            field=models.CharField(default='en stock', max_length=50, verbose_name=b'Estado', choices=[(b'no disponible', b'No disponible'), (b'preparando', b'Preparando'), (b'en stock', b'En Stock')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='order',
            name='saucer',
            field=models.ForeignKey(verbose_name=b'Platillo', to='locations.Saucer'),
        ),
    ]
