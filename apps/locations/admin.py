# -*- coding: utf-8 -*-
# Python imports


# Django imports
from django.contrib import admin


# Third party apps imports


# Local imports
from .models import Saucer, Order, Table


# Register your models here.
@admin.register(Saucer)
class SaucerAdmin(admin.ModelAdmin):

    list_display = ('name', 'price', 'quantity',)


@admin.register(Table)
class TableAdmin(admin.ModelAdmin):

    list_display = ('capacity', 'number',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):

    list_display = ('table',)
