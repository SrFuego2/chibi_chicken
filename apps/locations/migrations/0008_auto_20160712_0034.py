# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0007_auto_20160711_2310'),
    ]

    operations = [
        migrations.AlterField(
            model_name='saucer',
            name='price',
            field=models.DecimalField(verbose_name=b'Precio', max_digits=7, decimal_places=2),
        ),
    ]
