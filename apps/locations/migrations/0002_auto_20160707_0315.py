# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='saucer',
            name='state',
            field=models.CharField(max_length=50, verbose_name=b'Estado', choices=[(b'no disponible', b'No disponible'), (b'preparando', b'Preparando'), (b'en stock', b'En Stock')]),
        ),
    ]
