# -*- coding: utf-8 -*-
# Python imports


# Django imports
from django.apps import apps
from django.db import models


# Third party apps imports


# Local imports


# Create your managers here.
class OrderQuerySet(models.QuerySet):

    def by_table(self, table_id):
        return self.filter(table__id=table__id)
