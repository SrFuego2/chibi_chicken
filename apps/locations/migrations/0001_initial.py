# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name': 'Orden',
                'verbose_name_plural': 'Ordenes',
            },
        ),
        migrations.CreateModel(
            name='Saucer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=50, verbose_name=b'Nombre')),
                ('price', models.PositiveSmallIntegerField(verbose_name=b'Precio')),
                ('state', models.CharField(max_length=50, choices=[(b'no disponible', b'No disponible'), (b'preparando', b'Preparando'), (b'en stock', b'En Stock')])),
                ('quantity', models.PositiveSmallIntegerField(verbose_name=b'Cantidad')),
            ],
            options={
                'verbose_name': 'Platillo',
                'verbose_name_plural': 'Platillos',
            },
        ),
        migrations.CreateModel(
            name='Table',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('capacity', models.PositiveSmallIntegerField(verbose_name=b'Cantidad de personas')),
                ('number', models.PositiveSmallIntegerField(unique=True, verbose_name=b'Numero de mesa')),
            ],
            options={
                'verbose_name': 'Mesa',
                'verbose_name_plural': 'Mesas',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='saucers',
            field=models.ManyToManyField(to='locations.Saucer', verbose_name=b'Platillos'),
        ),
        migrations.AddField(
            model_name='order',
            name='table',
            field=models.ForeignKey(verbose_name=b'Mesa', to='locations.Table'),
        ),
    ]
