# -*- coding: utf-8 -*-
# Python imports


# Django imports
from django.conf.urls import url
from django.contrib.auth.views import login, logout


# Third party apps imports


# Local imports
from .views import (
    SaucersListView, SaucerCreateView, SaucerUpdateView, SaucerDeleteView,
    UsersListView, UserCreateView, UserDeleteView, login_waitress,
    OrderListView, OrderCreateView, order_send_kitchen, order_send_table,
    order_send_table_from_kitchen, OrderDeleteView)

# Create your tests here.


urlpatterns = [
    url(
        r'^platillos/$',
        SaucersListView.as_view(),
        name='index'
    ),
    url(
        r'^platillos/crear/$',
        SaucerCreateView.as_view(),
        name='saucer_create'
    ),
    url(
        r'^platillos/actualizar/(?P<id>\d+)/$',
        SaucerUpdateView.as_view(),
        name='saucer_update'
    ),
    url(
        r'^platillos/eliminar/(?P<id>\d+)/$',
        SaucerDeleteView.as_view(),
        name='saucer_delete'
    ),
    url(
        r'^usuarios/$',
        UsersListView.as_view(),
        name='users'
    ),
    url(
        r'^usuarios/crear/$',
        UserCreateView.as_view(),
        name='user_create'
    ),
    url(
        r'^usuarios/eliminar/(?P<id>\d+)/$',
        UserDeleteView.as_view(),
        name='user_delete'
    ),
    url(
        r'^$',
        login,
        name='login',
        kwargs={'template_name': 'main/login.html'}
    ),
    url(
        r'^logout/$',
        logout,
        name='logout',
        kwargs={'next_page': '/'}
    ),
    url(
        r'^login-mesero/$',
        login_waitress,
        name='login_waitress',
    ),
    url(
        r'^mesero/panel/(?P<table_selected>\d+)/$',
        OrderListView.as_view(),
        name='panel_waitress',
    ),
    url(
        r'^mesero/agregar-pedido/(?P<table_selected>\d+)/$',
        OrderCreateView.as_view(),
        name='panel_create',
    ),
    url(
        r'^mesero/mandar-cocina/(?P<table_selected>\d+)/(?P<order_id>\d+)/$',
        order_send_kitchen,
        name='send_kitchen',
    ),
    url(
        r'^mesero/mandar-mesa/(?P<table_selected>\d+)/(?P<order_id>\d+)/$',
        order_send_table,
        name='send_table',
    ),
    url(
        r'^cocinero/mandar-mesa/(?P<order_id>\d+)/$',
        order_send_table_from_kitchen,
        name='send_table_from_kitchen',
    ),
    url(
        r'^mesero/eliminar/(?P<table_selected>\d+)/(?P<id>\d+)/$',
        OrderDeleteView.as_view(),
        name='order_delete'
    ),
]
