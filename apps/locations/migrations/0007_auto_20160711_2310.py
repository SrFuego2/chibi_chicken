# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0006_auto_20160709_1248'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='state',
            field=models.CharField(max_length=50, verbose_name=b'Estado', choices=[(b'solicitado', b'Solicitado'), (b'preparando', b'Preparando'), (b'para servir', b'Para servir'), (b'cancelado', b'Cancelado'), (b'servido', b'Servido')]),
        ),
    ]
