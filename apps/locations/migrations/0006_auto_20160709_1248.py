# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0005_auto_20160708_1918'),
    ]

    operations = [
        migrations.AlterField(
            model_name='saucer',
            name='state',
            field=models.CharField(max_length=50, verbose_name=b'Estado', choices=[(b'no disponible', b'No disponible'), (b'agotado', b'agotado'), (b'en stock', b'En Stock')]),
        ),
    ]
