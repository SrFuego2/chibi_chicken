# -*- coding: utf-8 -*-
# Python imports


# Django imports
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import resolve_url
from django.template.response import TemplateResponse
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import CreateView, ListView, UpdateView, DeleteView


# Third party apps imports
from braces.views import LoginRequiredMixin


# Local imports
from apps.locations.models import Saucer, Table, Order


# Create your views here.
@sensitive_post_parameters()
@csrf_protect
@never_cache
def login_waitress(
        request, template_name='main/login_waitress.html',
        redirect_field_name=REDIRECT_FIELD_NAME,
        authentication_form=AuthenticationForm, current_app=None,
        extra_context=None):
    redirect_to = request.POST.get(
        redirect_field_name, request.GET.get(redirect_field_name, ''))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            if request.POST['table'] == 'Seleccionar Mesa':
                return redirect(reverse('main:login_waitress'))
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(reverse(
                    'main:panel_waitress',
                    kwargs={'table_selected': request.POST['table']}))
            auth_login(request, form.get_user())
            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    current_site = get_current_site(request)
    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
        'tables': Table.objects.all()
    }

    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)


class SaucersListView(LoginRequiredMixin, ListView):

    template_name = 'main/saucers.html'
    login_url = '/'
    context_object_name = 'saucers'
    model = Saucer
    queryset = Saucer.objects.all()

    def get_context_data(self, **kwargs):
        context = super(SaucersListView, self).get_context_data(**kwargs)
        context.update({
            'menu': 'saucers',
            'orders': Order.objects.all().order_by('table__number')
        })
        return context


class SaucerCreateView(LoginRequiredMixin, CreateView):

    model = Saucer
    fields = ('name', 'price', 'state', 'quantity',)

    def get_success_url(self):
        return reverse('main:index')

    def form_invalid(self, form):
        return redirect(reverse('main:index'))


class SaucerUpdateView(LoginRequiredMixin, UpdateView):

    model = Saucer
    fields = ('name', 'price', 'state', 'quantity',)
    pk_url_kwarg = 'id'
    template_name = 'main/saucers.html'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('main:index')

    def form_invalid(self, form):
        return redirect(reverse('main:index'))


class SaucerDeleteView(LoginRequiredMixin, DeleteView):

    model = Saucer
    pk_url_kwarg = 'id'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('main:index')


class UsersListView(LoginRequiredMixin, ListView):

    context_object_name = 'employees'
    model = User
    template_name = 'main/users.html'
    login_url = '/'

    def get_queryset(self):
        return User.objects.filter(
            groups__name__in=[
                'administradores', 'meseros', 'cocineros']).exclude(
                    id=self.request.user.id).order_by('username')

    def get_context_data(self, **kwargs):
        context = super(UsersListView, self).get_context_data(**kwargs)
        context.update({
            'menu': 'users'
        })
        return context


class UserCreateView(LoginRequiredMixin, CreateView):

    model = User
    fields = ('username', 'password', 'first_name', 'last_name', 'groups',)

    def get_success_url(self):
        return reverse('main:users')

    def form_invalid(self, form):
        return redirect(reverse('main:users'))

    def form_valid(self, form):
        try:
            user = User.objects.create_user(
                form.cleaned_data['username'], '',
                form.cleaned_data['password'])
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.groups = form.cleaned_data['groups']
            user.save()
        except Exception as e:
            print str(e)
        return redirect(reverse('main:users'))


class UserDeleteView(LoginRequiredMixin, DeleteView):

    model = User
    pk_url_kwarg = 'id'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('main:users')


class OrderListView(ListView):
    context_object_name = 'orders'
    model = Order
    template_name = 'main/orders.html'
    queryset = Order.objects.all().order_by('table__number')

    def get_context_data(self, *args, **kwargs):
        context = super(OrderListView, self).get_context_data(**kwargs)
        context.update({
            'table_selected': self.request.path_info.split('/')[3]
        })
        return context


class OrderCreateView(LoginRequiredMixin, CreateView):

    model = Order
    fields = ('table', 'saucer', 'quantity', 'state',)
    template_name = 'main/order_create.html'

    def get_success_url(self):
        return reverse(
            'main:panel_waitress',
            kwargs={'table_selected': self.request.path_info.split('/')[3]})

    def get_context_data(self, *args, **kwargs):
        context = super(OrderCreateView, self).get_context_data(**kwargs)
        context.update({
            'orders': Order.objects.all().order_by('table__number'),
            'saucers': Saucer.objects.all().order_by('name'),
            'table_selected': self.request.path_info.split('/')[3]
        })
        return context

    def form_valid(self, form):
        saucer = form.cleaned_data['saucer']
        order_quantity = form.cleaned_data['quantity']
        if saucer.quantity - order_quantity >= 0:
            saucer.quantity -= order_quantity
            if saucer.quantity == 0:
                saucer.state = 'agotado'
            saucer.save()
            self.object = form.save()
        else:
            return redirect(reverse(
                'main:panel_create',
                kwargs={'table_selected': self.path_info.split('/')[3]}))

        return super(OrderCreateView, self).form_valid(form)


@login_required
def order_send_kitchen(self, table_selected, order_id):
    order = Order.objects.get(id=order_id)
    order.state = 'preparando'
    order.save()
    return redirect(reverse(
        'main:panel_waitress',
        kwargs={'table_selected': table_selected}))


@login_required
def order_send_table(self, table_selected, order_id):
    order = Order.objects.get(id=order_id)
    order.state = 'servido'
    order.save()
    return redirect(reverse(
        'main:panel_waitress',
        kwargs={'table_selected': table_selected}))


@login_required
def order_send_table_from_kitchen(self, order_id):
    order = Order.objects.get(id=order_id)
    order.state = 'para servir'
    order.save()
    return redirect(reverse('main:index'))


class OrderDeleteView(LoginRequiredMixin, DeleteView):

    model = Order
    pk_url_kwarg = 'id'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse(
            'main:panel_waitress',
            kwargs={'table_selected': self.kwargs['table_selected']})
